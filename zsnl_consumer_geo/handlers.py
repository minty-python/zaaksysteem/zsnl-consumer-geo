# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.events import Event
from minty.exceptions import NotFound
from minty_amqp.consumer import BaseHandler

CUSTOM_OBJECT_ROUTING_KEY_PREFIX = (
    "zsnl.v2.zsnl_domains_case_management.CustomObject"
)


class GeoBaseHandler(BaseHandler):
    """
    Base class for all event handlers in the geo domain. Defines the "domain"
    property, required by `BaseHandler`.
    """

    @property
    def domain(self):
        return "zsnl_domains.geo"


class ObjectChangeHandler(GeoBaseHandler):
    """Handler for changes to custom objects, that updates the necessary
    entities in the `geo` domain."""

    @property
    def routing_keys(self):
        return [
            f"{CUSTOM_OBJECT_ROUTING_KEY_PREFIX}.CustomObjectCreated",
            f"{CUSTOM_OBJECT_ROUTING_KEY_PREFIX}.CustomObjectUpdated",
        ]

    def handle(self, event):
        changes = event.format_changes()

        try:
            cases = [case["uuid"] for case in changes["cases"]]
        except KeyError:
            cases = []

        command_instance = self.get_command_instance(event)

        try:
            command_instance.update_custom_object_geo(
                object_uuid=changes["uuid"],
                custom_fields=changes["custom_fields"],
                cases=cases,
            )
        except NotFound as e:
            self.logger.info(f"Object or object type not found: {e}")

        return


class ObjectDeleteHandler(GeoBaseHandler):
    """Handler for "CustomObjectDeleted" event
    that deletes corresponding geo features in geo domain.
    """

    @property
    def routing_keys(self):
        return [f"{CUSTOM_OBJECT_ROUTING_KEY_PREFIX}.CustomObjectDeleted"]

    def handle(self, event: Event):
        object_uuid = event.previous_value("version_independent_uuid")
        command_instance = self.get_command_instance(event)

        command_instance.delete_geo_feature(object_uuid=object_uuid)


class ObjectRelationChangeHandler(GeoBaseHandler):
    """
    Handler for "CustomObjectRelatedTo" and "CustomObjectUnrelatedFrom" events
    that mirrors changes to the appropriate links in the geo domain.
    """

    @property
    def routing_keys(self):
        return [
            f"{CUSTOM_OBJECT_ROUTING_KEY_PREFIX}.CustomObjectRelatedTo",
            f"{CUSTOM_OBJECT_ROUTING_KEY_PREFIX}.CustomObjectUnrelatedFrom",
        ]

    def handle(self, event: Event):
        old_cases = {case["uuid"] for case in event.previous_value("cases")}
        new_cases = {case["uuid"] for case in event.new_value("cases")}

        added_relations = list(new_cases - old_cases)
        removed_relations = list(old_cases - new_cases)

        command_instance = self.get_command_instance(event)

        # Geo feature relationships are added from the perspective of the
        # object, but the user expects them to show up on the map for the case
        # -- this is why the geo_feature_relationship is created "flipped":
        # that way it's correct from the perspective of the case.
        #
        # If we didn't flip them, all cases would show up on the map of the
        # object instead.
        for added in added_relations:
            command_instance.update_geo_feature_relationships(
                origin_uuid=added,
                added=[str(event.entity_id)],
                removed=[],
            )

        for removed in removed_relations:
            command_instance.update_geo_feature_relationships(
                origin_uuid=removed,
                added=[],
                removed=[str(event.entity_id)],
            )


class CaseRequestorChangeHandler(GeoBaseHandler):
    """
    Handler for changes to the requestor of a case.
    """

    @property
    def routing_keys(self):
        return ["zsnl.v2.legacy.Case.RequestorChanged"]

    def handle(self, event: Event):
        old_requestor_uuid = event.previous_value("requestor")
        new_requestor_uuid = event.new_value("requestor")

        command_instance = self.get_command_instance(event)
        command_instance.update_case_requestor(
            case_uuid=str(event.entity_id),
            old_requestor_uuid=old_requestor_uuid,
            new_requestor_uuid=new_requestor_uuid,
        )


class CaseChangeHandler(GeoBaseHandler):
    """Handler for changes to cases, to update the necessary entities in the
    `geo` domain."""

    @property
    def routing_keys(self):
        return ["zsnl.v2.legacy.Case.CaseCustomFieldsUpdated"]

    def handle(self, event):
        changes = event.format_changes()

        command_instance = self.get_command_instance(event)
        try:
            command_instance.update_case_geo(
                case_uuid=str(event.entity_id),
                custom_fields=changes["custom_fields"],
            )
        except NotFound as e:
            self.logger.info(f"Case not found: {e}")


class ContactUpdateHandler(GeoBaseHandler):
    """
    Handler that changes to contacts (persons, organizations).
    """

    @property
    def routing_keys(self):
        return [
            "zsnl.v2.legacy.Person.Updated",
            "zsnl.v2.legacy.Organization.Updated",
        ]

    def handle(self, event: Event):
        try:
            new_location = event.new_value("geolocation")
        except IndexError:
            self.logger.debug("Geolocation field not found in event")
            return

        new_location_geojson = {
            "type": "FeatureCollection",
            "features": [new_location],
        }

        try:
            command_instance = self.get_command_instance(event)
            command_instance.update_contact_geo(
                contact_uuid=str(event.entity_id), geojson=new_location_geojson
            )
        except Exception as e:
            self.logger.error(f"Error while processing event: {e}")

        return
