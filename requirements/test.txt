## Dependencies for QA (tests, documentation)
pytest~=6.0
pytest-spec~=3.0
pytest-cov~=2.10
pytest-mock~=3.3

# Code checks
black~=22.1
flake8~=3.8
isort~=5.0
safety
liccheck~=0.1
