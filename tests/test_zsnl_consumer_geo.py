# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
from minty.exceptions import NotFound
from unittest import mock
from uuid import uuid4
from zsnl_consumer_geo import __main__, handlers
from zsnl_consumer_geo.consumers import GeoConsumer


class TestConsumerGeo(unittest.TestCase):
    def test_consumer_routing(self):
        consumer = GeoConsumer.__new__(GeoConsumer)
        consumer.cqrs = "test"

        consumer._register_routing()

        self.assertEqual(len(consumer._known_handlers), 6)


class TestObjectChangeHandler(unittest.TestCase):
    def test_object_change_handler(self):
        mock_cqrs = mock.Mock()
        handler = handlers.ObjectChangeHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.geo")
        self.assertCountEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectCreated",
                "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectUpdated",
            ],
        )

        mock_command_instance = mock.Mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()
        object_uuid = uuid4()
        mock_event.format_changes.return_value = {
            "uuid": object_uuid,
            "custom_fields": {"custom": "field"},
            "cases": [],
        }
        handler.handle(mock_event)

        mock_event.format_changes.assert_called_once_with()
        mock_command_instance.assert_called_once_with(mock_event)
        mock_command_instance().update_custom_object_geo.assert_called_once_with(
            object_uuid=object_uuid,
            custom_fields={"custom": "field"},
            cases=[],
        )

    def test_object_change_handler_no_cases_in_event(self):
        mock_cqrs = mock.Mock()
        handler = handlers.ObjectChangeHandler(mock_cqrs)

        assert handler.domain == "zsnl_domains.geo"
        assert list(handler.routing_keys) == [
            "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectCreated",
            "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectUpdated",
        ]

        mock_command_instance = mock.Mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()
        object_uuid = uuid4()
        mock_event.format_changes.return_value = {
            "uuid": object_uuid,
            "custom_fields": {"custom": "field"},
        }
        handler.handle(mock_event)

        mock_event.format_changes.assert_called_once_with()
        mock_command_instance.assert_called_once_with(mock_event)
        mock_command_instance().update_custom_object_geo.assert_called_once_with(
            object_uuid=object_uuid,
            custom_fields={"custom": "field"},
            cases=[],
        )

    def test_object_change_handler_no_object(self):
        mock_cqrs = mock.Mock()
        handler = handlers.ObjectChangeHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.geo")
        self.assertCountEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectCreated",
                "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectUpdated",
            ],
        )

        mock_command_instance = mock.Mock()
        mock_command_instance().update_custom_object_geo.side_effect = NotFound
        mock_command_instance.reset_mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()
        object_uuid = uuid4()
        mock_event.format_changes.return_value = {
            "uuid": object_uuid,
            "custom_fields": {"custom": "field"},
            "cases": [],
        }
        handler.handle(mock_event)

        mock_event.format_changes.assert_called_once_with()
        mock_command_instance.assert_called_once_with(mock_event)
        mock_command_instance().update_custom_object_geo.assert_called_once_with(
            object_uuid=object_uuid,
            custom_fields={"custom": "field"},
            cases=[],
        )


class TestCaseChangeHandler(unittest.TestCase):
    def test_case_change_handler(self):
        mock_cqrs = mock.Mock()
        handler = handlers.CaseChangeHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.geo")
        self.assertCountEqual(
            handler.routing_keys,
            ["zsnl.v2.legacy.Case.CaseCustomFieldsUpdated"],
        )

        mock_command_instance = mock.Mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()
        case_uuid = uuid4()
        mock_event.entity_id = str(case_uuid)
        mock_event.format_changes.return_value = {
            "uuid": str(case_uuid),
            "custom_fields": {"custom": "field"},
        }
        handler.handle(mock_event)

        mock_event.format_changes.assert_called_once_with()
        mock_command_instance.assert_called_once_with(mock_event)
        mock_command_instance().update_case_geo.assert_called_once_with(
            case_uuid=str(case_uuid), custom_fields={"custom": "field"}
        )

    def test_case_change_handler_notfound(self):
        mock_cqrs = mock.Mock()
        handler = handlers.CaseChangeHandler(mock_cqrs)

        mock_command_instance = mock.Mock()
        mock_command_instance().update_case_geo.side_effect = NotFound
        mock_command_instance.reset_mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()
        case_uuid = uuid4()
        mock_event.entity_id = str(case_uuid)
        mock_event.format_changes.return_value = {
            "uuid": str(case_uuid),
            "custom_fields": {"custom": "field"},
        }
        handler.handle(mock_event)

        mock_event.format_changes.assert_called_once_with()
        mock_command_instance.assert_called_once_with(mock_event)
        mock_command_instance().update_case_geo.assert_called_once_with(
            case_uuid=str(case_uuid), custom_fields={"custom": "field"}
        )


class TestObjectRelationChangeHandler(unittest.TestCase):
    def test_object_relation_changed_event(self):
        mock_cqrs = mock.Mock()
        handler = handlers.ObjectRelationChangeHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.geo")
        self.assertCountEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectRelatedTo",
                "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectUnrelatedFrom",
            ],
        )

        mock_command_instance = mock.Mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()

        old_case_uuid = str(uuid4())
        new_case_uuid = str(uuid4())
        object_uuid = uuid4()

        mock_event.entity_id = str(object_uuid)
        mock_event.previous_value.return_value = [{"uuid": old_case_uuid}]
        mock_event.new_value.return_value = [{"uuid": new_case_uuid}]

        handler.handle(mock_event)

        mock_event.previous_value.assert_called_once_with("cases")
        mock_event.new_value.assert_called_once_with("cases")

        mock_command_instance.assert_called_once_with(mock_event)

        mock_command_instance().update_geo_feature_relationships.assert_any_call(
            origin_uuid=new_case_uuid, added=[str(object_uuid)], removed=[]
        )
        mock_command_instance().update_geo_feature_relationships.assert_any_call(
            origin_uuid=old_case_uuid,
            added=[],
            removed=[str(object_uuid)],
        )


class TestObjectDeleteHandler(unittest.TestCase):
    def test_object_delete_event(self):
        mock_cqrs = mock.Mock()
        object_uuid = str(uuid4())
        handler = handlers.ObjectDeleteHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.geo")
        self.assertCountEqual(
            handler.routing_keys,
            [
                "zsnl.v2.zsnl_domains_case_management.CustomObject.CustomObjectDeleted"
            ],
        )

        mock_command_instance = mock.Mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()
        mock_event.entity_id = uuid4()
        mock_event.previous_value.return_value = object_uuid

        handler.handle(mock_event)

        mock_event.previous_value.assert_called_once_with(
            "version_independent_uuid"
        )

        mock_command_instance.assert_called_once_with(mock_event)

        mock_command_instance().delete_geo_feature.assert_any_call(
            object_uuid=object_uuid
        )


class TestCaseRequestorChangeHandler(unittest.TestCase):
    def test_case_requestor_changed_event(self):
        mock_cqrs = mock.Mock()
        handler = handlers.CaseRequestorChangeHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.geo")
        self.assertCountEqual(
            handler.routing_keys,
            ["zsnl.v2.legacy.Case.RequestorChanged"],
        )

        mock_command_instance = mock.Mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()

        old_requestor_uuid = str(uuid4())
        new_requestor_uuid = str(uuid4())
        case_uuid = uuid4()

        mock_event.entity_id = case_uuid
        mock_event.previous_value.return_value = old_requestor_uuid
        mock_event.new_value.return_value = new_requestor_uuid

        handler.handle(mock_event)

        mock_event.previous_value.assert_called_once_with("requestor")
        mock_event.new_value.assert_called_once_with("requestor")

        mock_command_instance.assert_called_once_with(mock_event)

        mock_command_instance().update_case_requestor.assert_called_once_with(
            case_uuid=str(case_uuid),
            old_requestor_uuid=old_requestor_uuid,
            new_requestor_uuid=new_requestor_uuid,
        )


class TestContactUpdateHandler(unittest.TestCase):
    def test_contact_update_event(self):
        mock_cqrs = mock.Mock()
        handler = handlers.ContactUpdateHandler(mock_cqrs)

        self.assertEqual(handler.domain, "zsnl_domains.geo")
        self.assertCountEqual(
            handler.routing_keys,
            [
                "zsnl.v2.legacy.Person.Updated",
                "zsnl.v2.legacy.Organization.Updated",
            ],
        )

        mock_command_instance = mock.Mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()

        contact_uuid = uuid4()

        mock_event.entity_id = contact_uuid
        mock_event.new_value.return_value = "new_location"

        handler.handle(mock_event)

        mock_event.new_value.assert_called_once_with("geolocation")

        mock_command_instance.assert_called_once_with(mock_event)

        mock_command_instance().update_contact_geo.assert_called_once_with(
            contact_uuid=str(contact_uuid),
            geojson={
                "type": "FeatureCollection",
                "features": ["new_location"],
            },
        )

    def test_contact_update_event_no_geo(self):
        mock_cqrs = mock.Mock()
        handler = handlers.ContactUpdateHandler(mock_cqrs)

        mock_command_instance = mock.Mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()
        contact_uuid = uuid4()

        mock_event.entity_id = contact_uuid
        mock_event.new_value.side_effect = IndexError

        handler.handle(mock_event)

        mock_event.new_value.assert_called_once_with("geolocation")

        mock_command_instance.assert_not_called()

    def test_contact_update_event_invalid(self):
        mock_cqrs = mock.Mock()
        handler = handlers.ContactUpdateHandler(mock_cqrs)

        mock_command_instance = mock.Mock()
        mock_command_instance().update_contact_geo.side_effect = ValueError
        mock_command_instance.reset_mock()
        handler.get_command_instance = mock_command_instance

        mock_event = mock.Mock()

        mock_event.entity_id = None
        mock_event.new_value.return_value = "new_location"

        handler.handle(mock_event)

        mock_event.new_value.assert_called_once_with("geolocation")

        mock_command_instance.assert_called_once_with(mock_event)

        mock_command_instance().update_contact_geo.assert_called_once_with(
            contact_uuid=str(None),
            geojson={
                "type": "FeatureCollection",
                "features": ["new_location"],
            },
        )


class TestMain:
    @mock.patch("zsnl_consumer_geo.__main__.old_factory")
    def test_log_record_factory(self, old_factory):
        record = __main__.log_record_factory("a", test="b")

        old_factory.assert_called_once_with("a", test="b")
        assert record.zs_component == "zsnl_consumer_geo"
        assert record == old_factory()
